package com.southsystem.sistemabancario.validators;

import com.southsystem.sistemabancario.entitys.TipoPessoa;
import com.southsystem.sistemabancario.exceptions.NumeroDocumentoInvalidoException;

public  class NumeroDocumentoValidator {

    public static boolean isValid(String numDocumento, TipoPessoa tipo) {
        if (tipo.equals(TipoPessoa.PF) && numDocumento.length() != 11) {
            return false;
        }
        if (tipo.equals(TipoPessoa.PJ) && numDocumento.length() != 14) {
            return false;
        }
        return true;
    }

}
