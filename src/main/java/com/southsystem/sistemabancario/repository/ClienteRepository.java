package com.southsystem.sistemabancario.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.southsystem.sistemabancario.entitys.Cliente;

@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Long> {

}
