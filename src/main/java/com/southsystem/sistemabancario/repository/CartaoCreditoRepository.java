package com.southsystem.sistemabancario.repository;

import com.southsystem.sistemabancario.entitys.CartaoDeCredito;
import com.southsystem.sistemabancario.entitys.LimiteChequeEspecial;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CartaoCreditoRepository extends JpaRepository<CartaoDeCredito, Long> {
}
