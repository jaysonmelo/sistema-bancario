package com.southsystem.sistemabancario.repository;

import com.southsystem.sistemabancario.entitys.Cliente;
import com.southsystem.sistemabancario.entitys.ContaCorrente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContaRepository extends JpaRepository<ContaCorrente, Long> {
}
