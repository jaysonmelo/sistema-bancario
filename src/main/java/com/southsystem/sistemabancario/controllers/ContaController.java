package com.southsystem.sistemabancario.controllers;

import com.southsystem.sistemabancario.entitys.ContaCorrente;
import com.southsystem.sistemabancario.service.ContaService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/conta")
public class ContaController {

    private ContaService contaService;

    public ContaController(ContaService contaService) {
        this.contaService = contaService;
    }

    @GetMapping
    public List<ContaCorrente> getAllContas() {
        return contaService.getAllContas();
    }

}
