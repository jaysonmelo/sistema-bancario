package com.southsystem.sistemabancario.controllers;

import java.util.List;

import javax.validation.Valid;

import com.southsystem.sistemabancario.entitys.Cliente;
import com.southsystem.sistemabancario.exceptions.NumeroDocumentoInvalidoException;
import com.southsystem.sistemabancario.service.ClienteService;
import com.southsystem.sistemabancario.service.ClienteServiceImpl;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/api/cliente")
public class ClienteController {

	private ClienteService clienteService;
	
	public ClienteController(ClienteServiceImpl clienteService) {
		this.clienteService = clienteService;
	}

	@GetMapping
	public List<Cliente> getAllClientes() {
		return clienteService.getAllClientes();
	}

	@PostMapping
	public Cliente createCliente(@Valid @RequestBody Cliente cliente) {
		try {
			return clienteService.createCliente(cliente);
		} catch (NumeroDocumentoInvalidoException e) {
			throw new ResponseStatusException(
					HttpStatus.PRECONDITION_FAILED, "Número de dígitos do documento inválido", e);
		}
	}

}
