package com.southsystem.sistemabancario.entitys;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.southsystem.sistemabancario.validators.TipoEnumPattern;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
public class Cliente {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotBlank
	@NotNull(message = "Nome é obrigatório")
	private String nome;

	@TipoEnumPattern(regexp = "PF|PJ")
	@NotNull(message = "Tipo é obrigatório")
	private TipoPessoa tipo;

	@NotBlank
	@NotNull(message = "Documento é obrigatório")
	@Size(max = 14, min = 11)
	private String numDocumento;

	private Integer score;

	@JsonIgnore
	@OneToOne(mappedBy = "cliente")
	private ContaCorrente contaCorrente;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public TipoPessoa getTipo() {
		return tipo;
	}
	public void setTipo(TipoPessoa tipo) {
		this.tipo = tipo;
	}
	public String getNumDocumento() {
		return numDocumento;
	}
	public void setNumDocumento(String numDocumento) {
		this.numDocumento = numDocumento;
	}
	public Integer getScore() {
		return score;
	}
	public void setScore(Integer score) {
		this.score = score;
	}
	public ContaCorrente getContaCorrente() {
		return contaCorrente;
	}
	public void setContaCorrente(ContaCorrente contaCorrente) {
		this.contaCorrente = contaCorrente;
	}
}
