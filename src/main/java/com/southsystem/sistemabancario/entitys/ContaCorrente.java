package com.southsystem.sistemabancario.entitys;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.southsystem.sistemabancario.validators.TipoEnumPattern;

import javax.persistence.*;

@Entity
public class ContaCorrente {

	@Id
	private Long numero;

	private String agencia;

	@TipoEnumPattern(regexp = "C|E")
	private TipoContaCorrente tipo;

	@JsonIgnore
	@OneToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name = "cliente_id", referencedColumnName = "id")
	private Cliente cliente;

	@OneToOne(mappedBy = "conta")
	private LimiteChequeEspecial limiteChequeEspecial;

	@OneToOne(mappedBy = "conta")
	private CartaoDeCredito cartaoDeCredito;

	public Long getNumero() {
		return numero;
	}

	public void setNumero(Long numero) {
		this.numero = numero;
	}
	public String getAgencia() {
		return agencia;
	}
	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}
	public TipoContaCorrente getTipo() {
		return tipo;
	}
	public void setTipo(TipoContaCorrente tipo) {
		this.tipo = tipo;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public LimiteChequeEspecial getLimiteChequeEspecial() {
		return limiteChequeEspecial;
	}
	public void setLimiteChequeEspecial(LimiteChequeEspecial limiteChequeEspecial) {
		this.limiteChequeEspecial = limiteChequeEspecial;
	}
	public CartaoDeCredito getCartaoDeCredito() {
		return cartaoDeCredito;
	}
	public void setCartaoDeCredito(CartaoDeCredito cartaoDeCredito) {
		this.cartaoDeCredito = cartaoDeCredito;
	}
	public void setTipoContaByTipoPessoa(TipoPessoa tipoPessoa) {
		if (tipoPessoa.equals(TipoPessoa.PF)){
			this.tipo = TipoContaCorrente.C;
		}
		if (tipoPessoa.equals(TipoPessoa.PJ)){
			this.tipo = TipoContaCorrente.E;
		}
	}
}
