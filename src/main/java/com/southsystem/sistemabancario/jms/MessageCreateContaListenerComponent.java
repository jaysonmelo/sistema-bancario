package com.southsystem.sistemabancario.jms;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.southsystem.sistemabancario.entitys.Cliente;
import com.southsystem.sistemabancario.service.ContaService;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Component
public class MessageCreateContaListenerComponent {

    private JmsTemplate jmsTemplate;
    private ContaService contaService;

    public MessageCreateContaListenerComponent(JmsTemplate jmsTemplate, ContaService contaService) {
        this.jmsTemplate = jmsTemplate;
        this.contaService = contaService;
    }

    @JmsListener(destination = "queue.conta")
    public void onReceiverQueue(String message) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            Cliente cliente = mapper.readValue(message, Cliente.class);
            contaService.createConta(cliente);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

}