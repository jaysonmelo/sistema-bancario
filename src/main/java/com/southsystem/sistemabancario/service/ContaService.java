package com.southsystem.sistemabancario.service;

import com.southsystem.sistemabancario.entitys.*;
import com.southsystem.sistemabancario.repository.CartaoCreditoRepository;
import com.southsystem.sistemabancario.repository.ContaRepository;
import com.southsystem.sistemabancario.repository.LimiteChequeEspecialRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Random;

@Service
@PropertySource("classpath:conta.properties")
public class ContaService {
    public static final int MIN = 100000;
    public static final int MAX = 999999;
    private ContaRepository contaRepository;
    private LimiteChequeEspecialRepository limiteRepository;
    private CartaoCreditoRepository cartaoCreditoRepository;

    @Value("${contacorrente.agencia}")
    private String numeroAgencia;

    public ContaService(ContaRepository contaRepository, CartaoCreditoRepository cartaoCreditoRepository, LimiteChequeEspecialRepository limiteRepository) {
        this.contaRepository = contaRepository;
        this.limiteRepository = limiteRepository;
        this.cartaoCreditoRepository = cartaoCreditoRepository;
    }

    public List<ContaCorrente> getAllContas() {
        return contaRepository.findAll();
    }

    public void createConta(Cliente cliente) {
        ContaCorrente conta = new ContaCorrente();
        Integer numeroConta = new Random().nextInt(( MAX - MIN ) + 1 ) + MIN;
        conta.setNumero(numeroConta.longValue());
        conta.setTipoContaByTipoPessoa(cliente.getTipo());
        conta.setAgencia(numeroAgencia);
        conta.setCliente(cliente);
        conta = contaRepository.save(conta);
        createCartaoAndLimite(conta);
    }

    private void createCartaoAndLimite(ContaCorrente conta) {
        int score = conta.getCliente().getScore();
        LimiteChequeEspecial limite = new LimiteChequeEspecial();
        CartaoDeCredito cartao = new CartaoDeCredito();
        if (score != 1 || score != 0){
            if( score >= 2 && score <= 5){
                limite.setConta(conta);
                limite.setLimite(1000.00);
                cartao.setConta(conta);
                cartao.setLimite(200.00);
            }
            if( score >= 6 && score <= 8){
                limite.setConta(conta);
                limite.setLimite(2000.00);
                cartao.setConta(conta);
                cartao.setLimite(2000.00);
            }
            if( score == 9){
                limite.setConta(conta);
                limite.setLimite(5000.00);
                cartao.setConta(conta);
                cartao.setLimite(15000.00);
            }
            limiteRepository.save(limite);
            cartaoCreditoRepository.save(cartao);
        }
    }
}
