package com.southsystem.sistemabancario.service;

import java.util.List;
import java.util.Random;
import java.util.UUID;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.southsystem.sistemabancario.entitys.Cliente;
import com.southsystem.sistemabancario.entitys.TipoPessoa;
import com.southsystem.sistemabancario.exceptions.NumeroDocumentoInvalidoException;
import com.southsystem.sistemabancario.repository.ClienteRepository;
import com.southsystem.sistemabancario.validators.NumeroDocumentoValidator;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

@Service
public class ClienteServiceImpl implements ClienteService {

	private ClienteRepository clienteRepository;
	private JmsTemplate jmsTemplate;
	
	public ClienteServiceImpl(ClienteRepository clienteRepository, JmsTemplate jmsTemplate) {
		this.clienteRepository = clienteRepository;
		this.jmsTemplate = jmsTemplate;
	}

	public List<Cliente> getAllClientes() {
		return clienteRepository.findAll();
	}

	public Cliente createCliente(Cliente cliente) throws NumeroDocumentoInvalidoException{
		if (!NumeroDocumentoValidator.isValid(cliente.getNumDocumento(), cliente.getTipo())){
			throw new NumeroDocumentoInvalidoException();
		}
		cliente.setScore(new Random().nextInt(10));
		cliente = clienteRepository.save(cliente);
		createContaSendMessage(cliente);
		return cliente;
	}

	private void createContaSendMessage(Cliente cliente) {
		ObjectMapper mapper = new ObjectMapper();
		String clienteJson = null;
		try {
			clienteJson = mapper.writeValueAsString(cliente);
			jmsTemplate.convertAndSend("queue.conta", clienteJson);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
	}
}
