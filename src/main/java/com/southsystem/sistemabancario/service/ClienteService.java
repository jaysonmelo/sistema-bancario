package com.southsystem.sistemabancario.service;

import com.southsystem.sistemabancario.entitys.Cliente;
import com.southsystem.sistemabancario.exceptions.NumeroDocumentoInvalidoException;

import java.util.List;

public interface ClienteService {

    public List<Cliente> getAllClientes();
    public Cliente createCliente(Cliente cliente) throws NumeroDocumentoInvalidoException;

}
