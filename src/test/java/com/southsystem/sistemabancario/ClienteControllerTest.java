package com.southsystem.sistemabancario;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.southsystem.sistemabancario.controllers.ClienteController;
import com.southsystem.sistemabancario.entitys.Cliente;
import com.southsystem.sistemabancario.entitys.TipoPessoa;
import com.southsystem.sistemabancario.repository.ClienteRepository;
import com.southsystem.sistemabancario.service.ClienteService;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
//@WebMvcTest(ClienteController.class)
@AutoConfigureMockMvc
//@AutoConfigureTestDatabase
public class ClienteControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ClienteRepository repository;

    @Test
    public void quandoEntradaValida_entaoCriaCliente() throws IOException, Exception {
        Cliente maria = new Cliente();
        maria.setNome("Maria");
        maria.setNumDocumento("00125836914");
        maria.setTipo(TipoPessoa.PF);
        mvc.perform(post("/api/cliente").contentType(MediaType.APPLICATION_JSON).content(JsonUtil.toJson(maria)));

        List<Cliente> found = repository.findAll();
        assertThat(found).extracting(Cliente::getNome).contains("Maria");
    }

    @Test
    public void obterClientes_quandoGetClientes_entaoStatus200() throws Exception {

        // @formatter:off
        mvc.perform(get("/api/cliente").contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(greaterThanOrEqualTo(1))))
                .andExpect(jsonPath("$[0].nome", containsString("Maria")));
        // @formatter:on
    }
}
