package com.southsystem.sistemabancario;
import com.southsystem.sistemabancario.entitys.Cliente;
import com.southsystem.sistemabancario.entitys.TipoPessoa;
import com.southsystem.sistemabancario.exceptions.NumeroDocumentoInvalidoException;
import com.southsystem.sistemabancario.service.ClienteService;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ClienteServiceImplTest {

    @Autowired
    private ClienteService clienteService;

    @Test
    public void quandoClienteValido_entaoScoreEntreZeroENove() throws NumeroDocumentoInvalidoException {
        Cliente maria = new Cliente();
        maria.setNome("Maria");
        maria.setNumDocumento("01234567891");
        maria.setTipo(TipoPessoa.PF);
        maria = clienteService.createCliente(maria);

        assertThat(maria.getScore()).isBetween(0, 9);
    }

    @Test(expected = NumeroDocumentoInvalidoException.class)
    public void quandoDocumentoPFInvalido_entaoRetornaExcecao() throws NumeroDocumentoInvalidoException {
        Cliente maria = new Cliente();
        maria.setNome("Maria");
        maria.setNumDocumento("012345678911");
        maria.setTipo(TipoPessoa.PF);
        clienteService.createCliente(maria);
    }

    @Test(expected = NumeroDocumentoInvalidoException.class)
    public void quandoDocumentoPJInvalido_entaoRetornaExcecao() throws NumeroDocumentoInvalidoException {
        Cliente maria = new Cliente();
        maria.setNome("Maria LTDA");
        maria.setNumDocumento("001258369147251");
        maria.setTipo(TipoPessoa.PJ);
        clienteService.createCliente(maria);
    }

    @Test
    public void quandoClienteValido_entaoRetornaIdCliente() throws NumeroDocumentoInvalidoException {
        Cliente maria = new Cliente();
        maria.setNome("Maria LTDA");
        maria.setNumDocumento("00125836914721");
        maria.setTipo(TipoPessoa.PJ);
        maria = clienteService.createCliente(maria);

        assertThat(maria.getId()).isNotNull();
    }
}
